# Overview of the Metrics Parser

Python project that utilizes python-gitlab API to parse adoption metrics. Currently, we can use GraphQL to get some adoption metrics, but they only detail the number of projects that have DAST and SAST etc. enabled. This script lists the corresponding project names that have SAST and DAST enabled. We can also use Gainsight for this info, but the adoption is by user, not by project.

# How to use this script

1. Install [python-gitlab](https://github.com/python-gitlab/python-gitlab).
1. Create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
1. Replace PRIVATE_TOKEN and GROUP_ID variable values accordingly.
1. Run `python3 metrics_parser.py` on your Terminal.

# ToDo
- [ ] Output to a csv file instead of on screen
- [ ] Better error handling
- [ ] Refactoring for code cleanliness
- [x] Expand adoption metrics past SAST and DAST.

# Feature Request
https://gitlab.com/gitlab-org/gitlab/-/issues/350387
