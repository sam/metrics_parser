import gitlab

# Main variables
GROUP_ID = {INSERT_GROUP_ID}
PRIVATE_TOKEN = {INSERT_PAT}  # Create a PAT: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
REF_LIST = ['main', 'master', 'default', 'container', 'container-master']
CI_YML = '.gitlab-ci.yml'
DAST_TEMPLATE = "DAST.gitlab-ci.yml"
SAST_TEMPLATE = "Security/SAST.gitlab-ci.yml"
SECRET_DETECT_TEMPLATE = "Security/Secret-Detection.gitlab-ci.yml"

class ProjectInfo:
    def __init__(self, name=None, id=None, path=None):
        self.project_name = name
        self.project_id = id
        self.path = path
        self.dast_enabled = False
        self.sast_enabled = False
        self.secret_detect_enabled = False


def print_info(project_info_dict, num_projects):
    dast_projects = project_info_dict.get('dast')
    sast_projects = project_info_dict.get('sast')
    secret_detect_projects = project_info_dict.get('secret_detect')
    other_projects = project_info_dict.get('other')

    print("\nPROJECT LIST")
    print("========================================")
    print("\nProjects with DAST enabled:")
    for project in dast_projects:
        print(project.path)

    print("\nProjects with SAST enabled:")
    for project in sast_projects:
        print(project.path)

    print("\nProjects with Secret Detection enabled:")
    for project in secret_detect_projects:
        print(project.path)

    print("\nProjects without SAST, DAST, and Secret Detection")
    for project in other_projects:
        print(project.path)

    print("\nTOTALS")
    print("========================================")
    print("Total with DAST: " + str(len(dast_projects)))
    print("Total with SAST: " + str(len(sast_projects)))
    print("Total with Secret Detection: " + str(len(secret_detect_projects)))
    print("Total projects: " + str(num_projects))


def get_metrics(gl, projects_list):
    print("Getting metrics...")

    other_projects = []
    projects_with_dast = []
    projects_with_sast = []
    projects_with_secret_detect = []

    for project in projects_list:
        # Get individual project data
        project = gl.projects.get(project.id)

        print("Processing project: " + project.path_with_namespace)

        project_info = ProjectInfo(name=project.name, id=project.id, path=project.path_with_namespace)
        f = None

        global REF_LIST
        global CI_YML
        for ref in REF_LIST:
            try:
                # Attempt to get file from specific branch
                f = project.files.get(file_path=CI_YML, ref=ref)
            except Exception:
                continue
        if f is None:
            continue

        # Decode yml file
        yml_contents = str(f.decode())

        global DAST_TEMPLATE
        global SAST_TEMPLATE
        global SECRET_DETECT_TEMPLATE
        if DAST_TEMPLATE in yml_contents:
            project_info.dast_enabled = True
            projects_with_dast.append(project_info)
        if SAST_TEMPLATE in yml_contents:
            project_info.sast_enabled = True
            projects_with_sast.append(project_info)
        if SECRET_DETECT_TEMPLATE in yml_contents:
            project_info.secret_detect_enabled = True
            projects_with_secret_detect.append(project_info)
        if not project_info.sast_enabled and not project_info.dast_enabled and not project_info.secret_detect_enabled:
            other_projects.append(project_info)

    # Return dict with projects by feature enabled
    return {'other': other_projects, 'dast': projects_with_dast, 'sast': projects_with_sast,
            'secret_detect': projects_with_secret_detect}


def get_projects(gl):
    group = gl.groups.get(GROUP_ID)
    return group.projects.list(all=True, include_subgroups=True)


def authenticate():
    gl = gitlab.Gitlab('https://gitlab.com', private_token=PRIVATE_TOKEN)
    print("Begin authenticating...")
    gl.auth()
    print("Authentication successful!")
    return gl


def main():
    gl = authenticate()
    projects_list = get_projects(gl)
    project_info_dict = get_metrics(gl, projects_list)
    num_projects = len(projects_list)
    print_info(project_info_dict, num_projects)

main()
